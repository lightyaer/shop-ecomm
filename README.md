# E-commerce Apparel Shop

A Responsive E-Commerce Website with just Apparels Catalog.

#### This website is a clone of https://shop.polymer-project.org/


## To run this 

* Clone it 
* Open index.html


If the images don't load then,

* Install Node.js if not already installed
* Then run command "npm i -g live-server"
* Open the cloned directory in terminal
* Then run command "live-server"