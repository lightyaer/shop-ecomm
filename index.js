let categories = ["#mens-outerwear", "#ladies-outerwear", "#mens-tshirts", "#ladies-tshirts"];
function toggleCategories(category) {
    window.scrollTo(0, 0);
    $('#landing-page').css('display', 'none');
    $('#cart').css('display', 'none');

    categories.map(function (item) {
        if (item === category) {
            $(item).css('display', 'block');
            $(item).addClass('animate-fadein-blocks');
        } else {
            $(item).css('display', 'none');
        }
    });
}

function showLandingPage() {
    window.scrollTo(0, 0);
    $('#landing-page').css('display', 'block');
    $('#landing-page').addClass('animate-fadein-blocks');
    $('#apparel-details').css('display', 'none');
    $('.navbar').css('display', 'flex');
    $('#cart').css('display', 'none');

    categories.map(function (item) {
        $(item).css('display', 'none');
    });
}

function showSidebar() {
    $('.sidenav').css('width', '300px');
    $('.sidenav').css('border-right', '8px solid rgb(44, 44, 44)');
}

function hideSidebar() {
    $('.sidenav').css('width', '0');
    $('.sidenav').css('border-right', 'none');
}

function showSnackbar() {
    var snackbar = $('.snackbar');
    console.log(snackbar);
    snackbar.addClass('showSnackbar');
    setTimeout(function () {
        snackbar.removeClass('showSnackbar');
    }, 3000);
}

function onReady() {
    $('#title').click(function () {
        showLandingPage();
    });

    $('.menu-icon').click(function () {
        showSidebar();
    });

    $('.closebtn').click(function () {
        hideSidebar();
    });

    $('#mens-outerwear-nav, #mens-outerwear-sidebar ,#mens-outerwear-btn, .mens-outerwear-img').click(function () {
        toggleCategories('#mens-outerwear');
        hideSidebar();
    });

    $('#ladies-outerwear-nav, #ladies-outerwear-sidebar, #ladies-outerwear-btn, .ladies-outerwear-img').click(function () {
        toggleCategories('#ladies-outerwear');
        hideSidebar();
    });

    $('#mens-tshirts-nav, #mens-tshirts-sidebar, #mens-tshirts-btn, .mens-tshirts-img').click(function () {
        toggleCategories('#mens-tshirts');
        hideSidebar();
    });

    $('#ladies-tshirts-nav, #ladies-tshirts-sidebar, #ladies-tshirts-btn, .ladies-tshirts-img').click(function () {
        toggleCategories('#ladies-tshirts');
        hideSidebar();
    });

    $('.apparel-grid div .mens-hoodie, .apparel-grid div .ladies-jacket, .apparel-grid div .mens-tshirts, .apparel-grid div .ladies-tshirts').click(function (e) {
        window.scrollTo(0, 0);
        switch (e.target.className) {
            case "ladies-jacket":
                $('#apparel-details .apparel-img').css('background-image', "url('images/ladies_jacket.jpg')");
                $('#apparel-details .apparel-info div h3').html('Ladies Jacket');
                $('#apparel-details .apparel-info div price').html('$45.99');
                break;
            case "ladies-tshirts":
                $('#apparel-details .apparel-img').css('background-image', "url('images/ladies_tshirt.jpg')");
                $('#apparel-details .apparel-info div h3').html('Ladies T-shirt');
                $('#apparel-details .apparel-info div price').html('$17.50');
                break;
            case "mens-tshirts":
                $('#apparel-details .apparel-img').css('background-image', "url('images/mens_tshirt.jpg')");
                $('#apparel-details .apparel-info div h3').html('Mens T-shirt');
                $('#apparel-details .apparel-info div price').html('$17.50');
                break;
            case "mens-hoodie":
                $('#apparel-details .apparel-img').css('background-image', "url('images/mens_hoodie.jpg')");
                $('#apparel-details .apparel-info div h3').html('Mens Hoodie');
                $('#apparel-details .apparel-info div price').html('$50.90');
                break;

        }
        categories.map(function (item) {
            $(item).css('display', 'none');
        });
        $('.apparel-details-div').css('display', 'flex');
    });

    // Event for Cart Icon on the top left and Add to Cart Button on Apparel Details
    $('#apparel-details .apparel-info .btn, .header .cart-icon .material-icons').click(function () {
        $('#landing-page').css('display', 'none');
        categories.map(function (item) {
            $(item).css('display', 'none');
        });
        $('.navbar').css('display', 'none');
        $('.apparel-details-div').css('display', 'none');
    
        $('#cart').css('display', 'block');
    });

}

$(document).ready(onReady);

